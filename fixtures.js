const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('artists');
    await db.dropCollection('albums');
    await db.dropCollection('tracks');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [Nightwish, Prodigy] = await Artist.create({
    name: 'Nightwish',
    info: 'Nightwish',
    photo: 'Nightwish.jpg'
  }, {
    name: 'Prodigy',
    info: 'The Prodigy',
    photo: 'Prodigy.jpg'
  });

  const [Wishmaster, CenturyChild, TheFatOfTheLand, InvadersMustDie] = await Album.create({
    title: 'Wishmaster',
    artist: Nightwish._id,
    year: '2000',
    poster: 'Wishmaster.jpg'
  }, {
    title: 'Century child',
    artist: Nightwish._id,
    year: '2003',
    poster: 'Century_Child.jpg'
  }, {
    title: 'The Fat of the Land',
    artist: Prodigy._id,
    year: '1997',
    poster: 'The_Fat_Of_The_Land.jpg'
  }, {
    title: 'Invaders must die',
    artist: Prodigy._id,
    year: '2009',
    poster: 'Invaders_Must_Die.jpg'
  });

  await Track.create({
    name: 'Riddler',
    album: Wishmaster._id,
    duration: 285,
    number: 2
  }, {
    name: 'Wishmaster',
    album: Wishmaster._id,
    duration: 285,
    number: 6
  }, {
    name: 'Beauty and the Beast',
    album: CenturyChild._id,
    duration: 285,
    number: 2
  }, {
    name: 'Century Child',
    album: CenturyChild._id,
    duration: 285,
    number: 5
  }, {
    name: 'Breathe',
    album: TheFatOfTheLand._id,
    duration: 335,
    number: 2
  }, {
    name: 'Smack My Bitch Up',
    album: TheFatOfTheLand._id,
    duration: 343,
    number: 1
  }, {
    name: 'Serial Thrilla',
    album: TheFatOfTheLand._id,
    duration: 311,
    number: 5
  }, {
    name: 'Funky Shit',
    album: TheFatOfTheLand._id,
    duration: 317,
    number: 4
  }, {
    name: 'Climbatize',
    album: TheFatOfTheLand._id,
    duration: 398,
    number: 9
  }, {
    name: 'Narayan',
    album: TheFatOfTheLand._id,
    duration: 546,
    number: 7
  }, {
    name: 'Mindfields',
    album: TheFatOfTheLand._id,
    duration: 540,
    number: 6
  }, {
    name: 'Firestarter',
    album: TheFatOfTheLand._id,
    duration: 280,
    number: 8
  }, {
    name: 'Fuel My Fire',
    album: TheFatOfTheLand._id,
    duration: 259,
    number: 10
  }, {
    name: 'Diesel Power',
    album: TheFatOfTheLand._id,
    duration: 258,
    number: 3
  }, {
    name: 'Omen',
    album: InvadersMustDie._id,
    duration: 285,
    number: 2
  }, {
    name: 'Take Me to the Hospital',
    album: InvadersMustDie._id,
    duration: 285,
    number: 5
  });

  db.close();
});