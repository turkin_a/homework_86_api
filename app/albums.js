const express = require('express');
const Album = require('../models/Album');

const router = express.Router();

const createRouter = () => {
  router.get('/:id', (req, res) => {
    Album.find({artist: req.params.id}).sort({year: 1})
      .then(results => {
        res.send(results)
      })
      .catch(() => res.status(404).send({error: 'Album is not found'}));
  });

  return router;
};

module.exports = createRouter;