const express = require('express');
const Artist = require('../models/Artist');

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    Artist.find().sort({name: 1})
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;