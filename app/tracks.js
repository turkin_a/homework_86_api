const express = require('express');
const Track = require('../models/Track');

const router = express.Router();

const createRouter = () => {
  router.get('/:id', (req, res) => {
    Track.find({album: req.params.id}).sort({number: 1})
      .then(results => {
        res.send(results);
      })
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;